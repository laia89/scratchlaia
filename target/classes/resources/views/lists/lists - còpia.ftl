<!DOCTYPE html>
<html>
<head>
  <title>List Management</title>
   <!-- <script src="javascript/jquery-1.9.1.js"></script>
  <script src="javascript/lists.js"></script> -->
  <link rel="stylesheet" href="css/application.css" type="text/css" charset="utf-8">
</head>
<body>
  <div id="views">
    <div id="tasks">
      <h1>List Management</h1>
      
      <form action="/lists" method="POST">
        <input type="text" placeholder="Type the name of the new list" name="nameList">
        <!-- <input type="submit" value="Add your new list" name="commit" class="button"> -->
      </form>

      <div class="items"></div>


        <#list llistes.toList() as llista>
          <div class="item">
            <div class="view">
                <a href="/lists/${llista.getIdList()}"><span>${llista.getNameList()} i id=${llista.getIdList()} i size= ${llista.size()}</span></a>
                <a href="/lists/delete/${llista.getIdList()}"class="destroy"></a>                
            </div>
          </div>
        </#list>

        <div class="item">
            <div class="view">
                <a href="items.html"><span>My List (1)</span></a> <a class="destroy"></a>
            </div>
        </div>
        <div class="item">
            <div class="view">
                <a href="empty.html"><span>Empty List (0)</span></a><a class="destroy"></a>
            </div>
        </div>
    </div>
  </div>
</body>
</html>