<#import "/views/layout.ftl" as layout>
<@layout.ListLayout "Todos">  
    <h1>${tasques.getNameList()}</h1>
    
    <form action="/lists/${tasques.getIdList()}" method="POST">
      <input type="text" placeholder="What needs to be done?" name="nameTask">
    </form>
    
    <div class="items"></div>

      <#list tasques.toList() as tasca>
        <div class="item">
            <div class="view">
                <input type="checkbox">
                <span>${tasca.getNameTasca()}</span> 
                <a href="/lists/${tasques.getIdList()}/delete/${tasca.getIdTasca()}"class="destroy"></a>
            </div>
        </div>
      </#list>

    <footer>
      <a class="clear">Clear completed</a>
        <a href="/" class="clear">&lt; Return to list Management </a>
      <div class="count"><span class="countVal">1</span> left</div>
    </footer>
</@layout.ListLayout>
