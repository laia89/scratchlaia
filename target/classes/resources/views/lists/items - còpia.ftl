<!DOCTYPE html>
<html>
<head>
  <title>Todos</title>
  <link rel="stylesheet" href="../css/application.css" type="text/css" charset="utf-8">
</head>
<body>
  <div id="views">
    <div id="tasks">
      <h1>${tasques.getNameList()}</h1>
      
      <form action="/lists/${tasques.getIdList()}" method="POST">
        <input type="text" placeholder="What needs to be done?" name="nameTask">
      </form>
      
      <div class="items"></div>

        <#list tasques.toList() as tasca>
          <div class="item">
              <div class="view">
                  <input type="checkbox">
                  <span>${tasca.getNameTasca()}</span> 
                  <a href="/lists/${tasques.getIdList()}/delete/${tasca.getIdTasca()}"class="destroy"></a>
              </div>
          </div>
        </#list>

        <div class="item">
            <div class="view">
                <input type="checkbox">
                <span>Pending item</span> <a class="destroy"></a>
            </div>
        </div>
        <div class="item done">
            <div class="view">
                <input type="checkbox" checked="checked">
                <span>Completed Item</span> <a class="destroy"></a>
            </div>
        </div>

      <footer>
        <a class="clear">Clear completed</a>
          <a href="/" class="clear">&lt; Return to list Management </a>
        <div class="count"><span class="countVal">1</span> left</div>
      </footer>
    </div>
  </div>
</body>
</html>