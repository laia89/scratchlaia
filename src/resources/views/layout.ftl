<#macro ListLayout title="TodoList">
  <!DOCTYPE html>
  <html>
  <head>
    <title>${title}</title>
    <link rel="stylesheet" href="../css/application.css" type="text/css" charset="utf-8">
  </head>
  <body>
    <div id="views">
      <div id="tasks">
        <#nested/>
      </div>
    </div>
  </body>
  </html>
</#macro>