<#import "/views/layout.ftl" as layout>
<@layout.ListLayout "List Management">  
      <h1>List Management</h1>
      
      <form action="/lists" method="POST">
        <input type="text" placeholder="Type the name of the new list" name="nameList">
      </form>

      <div class="items"></div>

        <#list llistes.toList() as llista>
          <div class="item">
            <div class="view">
                <a href="/lists/${llista.getIdList()}"><span>${llista.getNameList()} (${llista.size()})</span></a>
                <a href="/lists/delete/${llista.getIdList()}"class="destroy"></a>                
            </div>
          </div>
        </#list>
</@layout.ListLayout>