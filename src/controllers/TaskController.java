package controllers;


import spark.TemplateEngine;
import static spark.Spark.*;
import infrastructure.LlistaRepository;

import java.util.ArrayList;
import java.util.HashMap;

import model.CollectionLlista;
import model.Llista;
import model.Tasca;

public class TaskController extends Controller {
	private final static String PATH_TO_VIEW = "views/lists/";
	
	private static LlistaRepository llistaRepository = LlistaRepository.getInstance();

	public TaskController(TemplateEngine templateEngine) {
		super(templateEngine, PATH_TO_VIEW);
		//The order is important for routing purposes
		detail();
		createTask();
		deleteTask();
	}
	
	private void detail() {
		get("/lists/:idList", (request, response) -> {
			int listId=Integer.parseInt(request.params(":idList"));
			Llista tasques=llistaRepository.findById(listId);
			return renderView("items.ftl", "tasques", tasques);		
	    }, getTemplateEngine());
	}
		
	
	private void createTask() {	
		post("/lists/:idList", (request, response) -> {
			int listId=Integer.parseInt(request.params(":idList"));
			Tasca newTask =new Tasca();
			newTask.setNameTasca(request.queryParams("nameTask"));
			newTask.setIdInLlista(listId);
			llistaRepository.saveTask(newTask);
			//Post/Redirect/Get Pattern
			response.status(301);
			response.redirect("/lists/"+newTask.getIdInLlista());
			return "";
        });
	}
	
	private void deleteTask() {
		get("/lists/:idList/delete/:idTasca", (request, response) -> {
			int listId=Integer.parseInt(request.params(":idList"));
			int taskId=Integer.parseInt(request.params(":idTasca"));
			Llista tasques=llistaRepository.findById(listId);
			tasques.deleteTaskById(taskId);
				response.status(404); // 404 Not found
				response.redirect("/lists/"+listId);
				return"";
		});	

	}
}
