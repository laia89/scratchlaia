package controllers;


import spark.TemplateEngine;
import static spark.Spark.*;
import infrastructure.LlistaRepository;

import java.util.ArrayList;
import java.util.HashMap;

import model.CollectionLlista;
import model.Llista;
import model.Tasca;

public class ListsController extends Controller {
	private final static String PATH_TO_VIEW = "views/lists/";
	
	private static LlistaRepository llistaRepository = LlistaRepository.getInstance();

	public ListsController(TemplateEngine templateEngine) {
		super(templateEngine, PATH_TO_VIEW);
		//The order is important for routing purposes
		index();	
		createList();
		deleteList();
	}

	private void index() {
		get("/lists", (request, response) -> {
			CollectionLlista llistes=llistaRepository.findAll();
			return renderView("lists.ftl", "llistes", llistes);
        }, getTemplateEngine());
	}

	private void createList() {	
		post("/lists", (request, response) -> {
			Llista newLlista =new Llista();
			newLlista.setNameList(request.queryParams("nameList"));
			llistaRepository.saveList(newLlista);
			
			//Post/Redirect/Get Pattern
			response.status(301);
			response.redirect("/lists");
			return "";
        });
	}

	
	private void deleteList() {
		get("/lists/delete/:idList", (request, response) -> {
			int listId=Integer.parseInt(request.params(":idList"));
			llistaRepository.deleteListById(listId);
				response.status(404); // 404 Not found
				response.redirect("/lists");
				return"";
		});	

	}

}
