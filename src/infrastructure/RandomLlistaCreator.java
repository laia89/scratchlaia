package infrastructure;

import java.util.Random;

import model.CollectionLlista;
import model.Llista;
import model.Tasca;


public class RandomLlistaCreator {

	
	public static CollectionLlista create ( int count ) {
		CollectionLlista llistes = new CollectionLlista();
		for (int i = 0; i < count; i++) {
			Llista randomLlista = createRandomNameLlista();
			llistes.add(randomLlista);
		}
		return llistes;
	}
	
	private static Llista createRandomNameLlista() {
		Llista llista = new Llista();
		llista.setNameList(randomNameLlista());
		int count = 6;
		for (int i = 0; i < count; i++){
			Tasca randomLlista = createRandomNameTasca();
			llista.addTask(randomLlista);
		}
		return llista;
	}
	
	
	private static Tasca createRandomNameTasca(){
		Tasca tasca = new Tasca();
		tasca.setNameTasca("Tasca"+randomNameLlista());
		return tasca;
	}

	private static Random random = new Random();

	private static int generateRandomInt(int min, int max) {
		return random.nextInt((max - min) + 1) + min;
	}

	private static String randomNameLlista() {
		switch (generateRandomInt(1, 7)) {
		case 1:
			return "Barcelona";
		case 2:
			return "Berlin";
		case 3:
			return "NY city";
		case 4:
			return "Chicago";
		case 5:
			return "San Francisco";
		case 6:
			return "Moscow";
		}
		return "London";
	}
	


}
