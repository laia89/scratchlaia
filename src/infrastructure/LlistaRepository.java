package infrastructure;

//import models.Job;
//import models.Category;
import model.CollectionLlista;
import model.Llista;
import model.Tasca;

public class LlistaRepository {

	private static CollectionLlista llistes=RandomLlistaCreator.create(3);
	
	private LlistaRepository() {
		
	}
	
	public CollectionLlista findAll() {
		return llistes;
	}
	
	public void saveList(Llista newLlista) {
		llistes.add(newLlista);
	}
	
	public void saveTask (Tasca newTask) {
		llistes.addTask(newTask);
	}
	
	public void deleteListById (int id) {
		Llista deletedLlista = findById(id);
		llistes.remove(deletedLlista);
	}
	
	public Llista findById (int id) {
		return llistes.findTasquesById(id);
	}
	
	
	private static LlistaRepository _LlistaRepository=null;
	public static LlistaRepository getInstance() {
		if (_LlistaRepository==null) {
			_LlistaRepository=new LlistaRepository();
		}
		return _LlistaRepository;
	}

}
