package model;
import java.util.ArrayList;


public class CollectionLlista {
	
	private ArrayList<Llista> llistes = new ArrayList<Llista>();
	
	public CollectionLlista () {
	}
	
	
	public void add(Llista llista){
		llistes.add(llista);
	}
	
	public void remove (Llista llista){
		llista.removeAllTask();
		llistes.remove(llista);
	}
	
	public ArrayList<Llista> toList() {
		return llistes;
	}


	public Llista findTasquesById(int id) {
		for(Llista llista : llistes) {  
			if (llista.has(id)) {
				return llista;
			}
		}
		return null;
	}

	public void addTask (Tasca tasca){
		for (Llista llista:llistes) {
			if(tasca.has(llista)){
				llista.addTask(tasca);
				return;
			}
		}
	}

}
