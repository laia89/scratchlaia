package model;

import java.util.ArrayList;

public class Llista {
	
	private static int count = -1;
	
	private int idList;
	private String nameList;
	private ArrayList<Tasca> tasques = new ArrayList<Tasca>();
		
	
	public Llista() {
		count++;
		idList = count; 
	}
	

	public int getIdList() {
		return idList;
	}

	public void setIdList(int idList) {
		this.idList = idList;
	}

	public String getNameList() {
		return nameList;
	}

	public void setNameList(String nameList) {
		this.nameList = nameList;
	}
	
	
	
	//ArrayList Tasques
	public void addTask (Tasca tasca){
		tasques.add(tasca);
	}

	public ArrayList<Tasca> toList(){
		return tasques;
	}
	
	public int size() {
		return tasques.size();
	}


	public boolean has (int id) {
		return this.idList == id;
	}


	public void removeAllTask() {
		tasques.clear();
		
	}


	public void deleteTaskById(int taskId) {
		Tasca tasca = findTascaById(taskId);
		tasques.remove(tasca);
	}


	private Tasca findTascaById(int taskId) {
		for(Tasca tasca : tasques) {  
			if (tasca.getIdTasca() == taskId) {
				return tasca;
			}
		}
		return null;
	}


	

}
