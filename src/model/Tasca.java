package model;

public class Tasca {
	private static int count = -1;
	
	private int idTasca;
	private String nameTasca;
	private int idInLlista;
	
	
	public int getIdInLlista() {
		return idInLlista;
	}


	public void setIdInLlista(int idInLlista) {
		this.idInLlista = idInLlista;
	}


	public Tasca() {
		count++;
		idTasca = count; 
	}


	public int getIdTasca() {
		return idTasca;
	}


	public void setIdTasca(int idTasca) {
		this.idTasca = idTasca;
	}


	public String getNameTasca() {
		return nameTasca;
	}


	public void setNameTasca(String nameTasca) {
		this.nameTasca = nameTasca;
	}

	public boolean has (Llista llista) {
		return this.idInLlista == llista.getIdList();
	}
	
}
